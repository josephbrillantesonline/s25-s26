// [SECTION] Creating a Basic Server Setup

// Node JS => Will provide us with a "runtime environment (RTE)" that will allow us to execute our program/application.

// RTE (Runtime Environment) -> we are pertaining to an environment/system in which a program can be executed.

// TASK: let's create a std server setup using 'plain' 

// 1. Identify and prepare the components and ingredients that you wqould need in order to execute the task.

	// the main ingredient when creating a server using 'plainh node is: http (hypertext transfer protocol)
	// why HTTP the main ingredient?
		//  HTTP is a 'built in' module of NODE JS, that will allow us to establish a connection and will make it possible to transfer data over HTTP.

	// you need to be able to get the components first, 
	// we will use the require() directive => the function that will allow us to gather and acquire certain packages that we will be using to build our application.
	const http = require('http')
		// http will provide us with all the components needed to establish a server.

		// http contains the utility constructor called 'createServer()' = > w/c will allow us to create an HTTP server.

// 2. Identify and describe a location where the connection will happen, in order to provide a proper medium for both parties involved {clients, server}, we will the find the connection to the desired port number
let port = 3000;
// 3. Inside the server constructor, insert a method in w/c we can use in order to describe the connection that was established. Identify the interaction b/w the client 
http.createServer((request, response) => {
// 4. Bind/assign the conenction to the designated address, the listen() function is used to bind and listen to a specific port whenever  its being accessed by the computer
	// write() => this will allow us to insert messages or inputs to our page.
	response.write('Hello? ${port}'); 
	response.write('Its me');
	response.write('Adele');
	response.end();
	// end() will allow us to identify a point where the transmission of data will end.

}).listen(port);



console.log('Server is Running');

// NOTE: when creating a project we shoulld always think of more efficient ways in executing even the most smallest task.

// TASK: Integrate the use of an NPM Project in NodeJS

	// 1. INITIALIZE an NPM (Node Package Manager) into the local projet.
		// (2 ways to perform the task)
			// 1: npm init
			// package.json => 'The heart of any NODE projects'. It records all the important metadata about the project, (libraries, )


		// Now, that we have integrated an NPM into our project, lets solve some issues we are facing in our local project.
			// issue: needing to restart the project whenever changes occur.
			// solve: using NPM lets install a dependency that will allow to automatically fix (hotfix) changes in out app.

			// => 'nodemon' package

			// BASIC SKILL using NPM
			// => install package
				// 1st method= npm install <name of dependencies or pkg>
				// 2nd method= npm i <name of pkg>

			// NOTE: you can also install multiple pkgs at the same time
				// nodemon
				// express
				// bcrypt

			// => uninstall or remove a package
				// 1st method= npm uninstall <name of pkg>
				// 2nd method= npm un <name of pkg>
			
			// nodemon utility => is a CLI utility tool, it's task is to wrap your node js app and watch for any changes in the file system and automatically restrat/hotfix the process.

			// If this is the 1st time your local machine would use a platform / technology, the machine would need to recognize the app/ technology first

			// ISSUE: command not found

			// YOU INSTALL IT ON A 'GLOBAL SCALE'
			// Global => the file system structure of the whole machine
			// npm install -g nodemon
			// nodemon server

			// After installing the new pkg, register it to the package.json file.

			// customize commands
				// to use npm run <custom name>
